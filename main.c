// Vamos definir um nó da lista linkada
#include <stdlib.h>
#include <stdio.h>

//Vamos criar um tipo do nosso nó
typedef struct node { // typedef serve para criar um apelido para nosso nó
  int valor;
  struct node * proximo;
} node_t;


// Iterar sobre a lista e imprimir cada elemento
void print_list(node_t * head){
  /*
   * */
  node_t * node_atual = head; // Nosso nó atual vai ser o primeiro da lista
  while (node_atual != NULL) { // Caso o nó não exista não vai alterar nada
    printf("%d\n", node_atual->valor); // Imprime o valor do nosso nó
    node_atual = node_atual->proximo; // O próximo elemento vai ser o nosso nó
  } // Então entramos em loop até o próximo não existir mais. :-D
} // Muito simples!!

// Adicionar um item ao fim da lista
void push(node_t ** head, int valor) {
  /*
   * 1. Crie um novo nó e define o valor
   * 2. Vincular o nó criado e apontar para o ponteiro do cabeçalho
   * 3. Definir o topo da lista para ser o novo nó
   *
   * */
  node_t * novo_node; // 1
  novo_node = (node_t *) malloc(sizeof(node_t)); // 2

  // 3. --------------------
  novo_node->valor = valor;
  novo_node->proximo = *head;  
  *head = novo_node;
  // -----------------------
}

// Remove o primeiro item
int pop(node_t ** head) {
  /* 
   * 1. Pegue o prómixo item para o qual o cabeçalho aponta
   * 2. Liberação da memória do ponteiro do cabeçalho
     * 3. Define o cabeçalho para o próximo item
   * */
  int retval = -1;
  node_t * proximo_node = NULL;

  if(*head == NULL) {
    return -1;
  }
  proximo_node = (*head)->proximo;
  retval = (*head)->valor;
  free(*head);
  *head = proximo_node;
  return retval;
}

// Remove o últime item da lista
int remove_last(node_t * head) {
  int retval = 0;
  // Se apenas existir um item na lista no momento
  if(head->proximo == NULL) {
    retval = head->valor;
    free(head);
    return retval;
  }
  // Percorrer a partir do segundo item da lista até o final
  node_t * atual = head;
  while(atual->proximo->proximo != NULL) {
    atual = atual->proximo;
  }
  // Agora, o ponteiro aponta para o penúltimo item da lista.
  retval = atual->proximo->valor;
  free(atual->proximo);
  atual->proximo = NULL;
  return retval;
}

int remove_by_index(node_t ** head, int n) {
  int i = 0;
  int retval = -1;
  node_t *current = *head;
  node_t *temp_node = NULL;

  if(n == 0) {
    return pop(head);
  }

  for(i = 0; i < n-1; i++) {
    if(current->proximo == NULL) {
      return -1;
    }
    current = current->proximo;
  }
  temp_node = current->proximo;
  retval = temp_node->valor;
  current->proximo = temp_node->proximo;
  free(temp_node);

  return retval;
}

int main(int argc, char *argv[]){
  node_t * test_list = (node_t *) malloc(sizeof(node_t)); // Alocando memória para o primeiro valor

  test_list->valor = 1; // Inserindo o primeiro valor
  test_list->proximo = (node_t *) malloc(sizeof(node_t)); // Fazendo alocação memória para o segundo valor
  test_list->proximo->valor = 2; // Inserindo o segundo valor


  print_list(test_list);

  return EXIT_SUCCESS;
}
